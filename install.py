from os import system as runcmd
import platform
if "Linux" in platform.platform():
    installDirectory = "/var/termatiko"
elif "Darwin" in platform.platform():
    if "iPad" in platform.platform():
        installDirectory = "/private/var/mobile/Containers/Shared/AppGroup/DCEEE3E0-C86D-4869-AC70-44256556A6F4/File Provider Storage/termatiko"
    else:
        installDirectory = "/var/termatiko"
elif "Windows" in platform.platform():
