#!/usr/bin/env python3
from os import system as runcmd
from datetime import date
from hashlib import sha256
h = sha256()
runcmd("python3 -m pip install termcolor")
from termcolor import colored
def clear():
    clearTimeCounter = 0
    while clearTimeCounter != 101:
        print("\n")
        clearTimeCounter = (clearTimeCounter + 1)
    main()
def main():
    currentCommand = input("$ ")
    currentCommandLenght = len(currentCommand)
    if "createFile" in currentCommand:
        fileName = currentCommand[11:(currentCommandLenght)]
        fileToCreate = open("./userData/piciakk/" + fileName, "x")
        filetoCreate.write("")
        main()
    elif "editFile" in currentCommand:
        fileName = currentCommand[8:(currentCommandLenght)]
        fileToEdit = open("./userData/piciakk/" + fileName, "x")
        main()
    elif "pkg" in currentCommand:
        main()
    elif "start" in currentCommand:
        programToStart = currentCommand[7:(currentCommandLenght)]
        main()
    elif "clear" in currentCommand:
        clear()
    elif "exit" in currentCommand:
        exit()
    elif "downloadFile" in currentCommand:
        url = currentCommand[13:(currentCommandLenght)]
        urlLenght = len(url)
        runcmd("wget " + url)
        slashPos = url.find("/")
        fileName = url[slashPos:(urlLenght)]
        runcmd("mv ./" + fileName + " ./userData/" + currentUser + "/")
        print(colored("Download of " + fileName + " completed!", "green"))
        main()
    elif "gui" in currentCommand:
	    print("Press the spacebar and click here")
    else:
        print(colored("I'm sorry, but I don't know this command", "red"))
        main()
print(colored("Please login or register!", "red"))
currentCommand = input()
currentCommandLenght = len(currentCommand)
if "register" in currentCommand:
    loginUsername = currentCommand[9:(currentCommandLenght)]
    loginPassword = input("Please enter the password: ")
    h.update(bytes(loginPassword, encoding='utf-8'))
    hash = h.hexdigest()
    loginFile = open("./loginData/" + loginUsername + ".txt", "x")
    loginFile.write(hash)
    loginFile.close()
    runcmd("mkdir ./userData/" + loginUsername)
    userDataFile = open("./userData/" + loginUsername + "/data.txt", "x")
    userDataFile.write("created: " + str(date.today()))
    userDataFile.close()
    currentUser = loginUsername
    main()
elif "login" in currentCommand:
    loginUsername = currentCommand[6:(currentCommandLenght)]
    chancePassword = input("Please enter the password: ")
    h.update(bytes(chancePassword, encoding='utf-8'))
    chancePassword = h.hexdigest()
    passwordFile = open("./loginData/" + loginUsername + ".txt", "r")
    password = passwordFile.readline()
    if chancePassword == password:
        print("You have entered the good password!")
        print(colored("Successful login as " + loginUsername + "!", "green"))
    currentUser = loginUsername
    main()
